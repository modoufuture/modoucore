<?php
$lang = __DIR__.'/lang/'.\think\facade\Request::langset().'.php';
if (!is_file($lang)) {
    $lang = __DIR__.'/lang/zh-cn.php';
}
\think\facade\Lang::load($lang);
\think\Console::addDefaultCommands([
    'install' => '\modoufuture\modoucore\command\Install',
]);
if (!function_exists('mconfig'))
{
    /**
     * 获取框架配置信息
     * @param string $name
     * @param null|mixed   $default
     *
     * @return mixed
     */
    function mconfig($name='', $default = null)
    {
        $name = trim($name, '.');
        $appConfig = app()->config;
        if ($appConfig->has('modou')) {
            return $appConfig->get('modou.'.$name, $default);
        }
        $file = app()->getRuntimePath().'config/vision.php';
        if (is_file($file)) { // 有集合的代码
            $appConfig->load($file, 'modou');
        } else { // 没有集合的代码
            $defaultConfig = include __DIR__.'/config.php';
            $flmConfig = $appConfig->get('vision.', []);
            $config = array_merge($defaultConfig, $flmConfig);
            $appConfig->set($config, 'modou');
        }

        return $appConfig->get('modou.'.$name, $default);
    }

}
if (!function_exists('success')){
    /**
     * 成功
     * @param string $msg
     * @param array $data
     * @param array $append
     * @return array
     */
    function success($msg='success', $data=[], $append=[], $token=true)
    {
        $return = ['code'=>1, 'msg'=>t($msg), 'data'=>$data];
        $append && is_array($append) && $return += $append;
        if ($token) {
            $return['token'] = \think\facade\Request::token();
        }

        return $return;
    }
}
if (!function_exists('failed')) {
    /**
     * 失败
     * @param string|mixed $msg
     * @param array $data
     * @param array $append
     * @return array
     */
    function failed($msg='failed', $data=[], $append=[], $token=true)
    {
        $return = ['code'=>1, 'msg'=>t('failed'), 'data'=>$data];
        if ($msg instanceof \Exception || $msg instanceof \Throwable) {
            $return['errcode'] = $msg->getCode();
            $return['msg'] = $msg->getMessage();
        } elseif (is_object($msg)) {
            $return['msg'] = $msg->getError();
        } else {
            $return['msg'] = t($msg);
        }
        $append && is_array($append) && $return += $append;
        if ($token) {
            $return['token'] = \think\facade\Request::token();
        }

        return $return;
    }
}
if (!function_exists('api_success')) {
    /**
     * @param string $msg
     * @param array  $data
     * @param array  $append
     * @param bool   $token
     *
     * @return \think\response\Json
     */
    function api_success($msg='success', $data=[], $append=[], $token=true)
    {
        empty($append['errcode']) && $append['errcode'] = '10000';
        empty($append['errmsg']) && $append['errmsg'] = t($msg);

        $return = success($msg, $data, $append, $token);

        return json($return);
    }
}
if (!function_exists('api_failed')) {
    /**
     * @param string $msg
     * @param integer|string  $code
     * @param array  $data
     * @param array  $append
     * @param bool   $token
     *
     * @return \think\response\Json
     */
    function api_failed($msg='failed', $code='0',  $data=[], $append=[], $token=true)
    {
        $append['code'] = $code;
        empty($append['errmsg']) && $append['errmsg'] = t($msg);

        $return = failed($msg, $data, $append, $token);

        return json($return);
    }
}
if(!function_exists('t')) {
    /**
     * 获取语言变量值
     * @param string $name 语言变量名
     * @param array $vars 动态变量值
     * @param string $lang 语言
     * @return mixed
     */
    function t($name, $vars = [], $lang = '')
    {
        if (is_numeric($name) || !$name) {
            return $name;
        }
        if (is_array($name)) {
            $vars = $name;
            $name = array_shift($vars);
        }
        if (!is_array($vars)) {
            $vars = func_get_args();
            array_shift($vars);
            $lang = '';
        }

        return \think\facade\Lang::get($name, $vars, $lang);
    }
}
if (!function_exists('get_class_name')) {
    /**
     * @param      $class
     * @param bool $namespace
     * @param int  $type
     * @param bool $ucfirst
     *
     * @return array|mixed|string
     */
    function get_class_name($class, $namespace=false, $type = 1, $ucfirst = true)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        $class = str_replace('/', '\\', $class);
        if ($namespace) {
            return $class;
        }
        $class = explode('\\', $class);
        $class = array_pop($class);

        return \think\Loader::parseName($class, $type, $ucfirst);
    }
}
if (!function_exists('get_model')) {
    /**
     * @param      $name
     * @param bool $singleton
     *
     * @return object|\modoufuture\modoucore\common\model\Common
     */
    function get_model($name, $singleton = true)
    {
        return make_class($name, 'model', function($baseName, $className){
            return new \modoufuture\modoucore\common\model\Common([], $baseName);
        }, $singleton);
    }
}
if (!function_exists('get_validate')) {
    /**
     * @param      $name
     * @param bool $singleton
     *
     * @return object|\modoufuture\modoucore\common\validate\Common
     */
    function get_validate($name, $singleton = true)
    {
        return make_class($name, 'validate', function($baseName, $className){
            return new \modoufuture\modoucore\common\validate\Common();
        }, $singleton);
    }
}
if (!function_exists('get_logic')) {
    /**
     * @param      $name
     * @param bool $singleton
     *
     * @return object|\modoufuture\modoucore\common\logic\Common
     */
    function get_logic($name, $singleton = true)
    {
        return make_class($name, 'logic', function($baseName, $className){
            return new \modoufuture\modoucore\common\logic\Common($baseName);
        }, $singleton);
    }
}
if (!function_exists('make_class')) {
    /**
     * @param      $name
     * @param      $layer
     * @param null $callback
     * @param bool $singleton
     *
     * @return object|\StdClass
     */
    function make_class($name, $layer, $callback=null, $singleton=true)
    {
        $baseName = get_class_name($name); // User
        $className = is_object($name) ? $baseName : $name; // User or admin/User
        $guid = $className.'.'.$layer;
        if ($singleton && app()->has($guid)) {
            return app()->get($guid);
        }
        try {
            $object = app()->create($name, $layer);
        } catch (\Exception $e) {
            try {
                $object = app()->create('common/'.$className, $layer);
            } catch (\Exception $e) {
                if (empty($callback) || !is_callable($callback)) {
                    $object = new \StdClass();
                } else {
                    $object = $callback($baseName, $className);
                }
                app()->bindTo($guid, $object);
            }
        }
        if ($singleton) {
            app()->delete($guid);
        }

        return $object;
    }
}
if (!function_exists('cache_get')) {
    function cache_get($key, $default = null, $expire = null, $tag = null)
    {
        if (\think\facade\Cache::has($key)) {
            return \think\facade\Cache::get($key);
        }
        if (is_null($default)) {
            return null;
        } elseif ($default instanceof \Closure || function_exists($default)) {
            $default = $default();
            \think\facade\Cache::set($key, $default, $expire);
            $tag && \think\facade\Cache::tag($tag, $key);

            return $default;
        }

        return null;
    }
}