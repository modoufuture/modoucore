<?php

namespace modoufuture\modoucore\library;

use modoufuture\utils\Dir;
use modoufuture\utils\Str;

class DbConfig
{
    /**
     * @param $dir
     * @param $config
     *
     * @return mixed
     */
    public static function import($dir, &$config)
    {
        $dir = rtrim(str_replace('\\', '/', $dir), '/');
        $lists = Dir::scan($dir, true, ['ext'=>['php']]);
        foreach($lists as $file) {
            $file = $dir.'/'.$file;
            self::set(basename($file), $file, $config);
        }

        return $config;
    }

    /**
     * @param $name
     * @param $value
     * @param $config
     *
     * @return mixed
     */
    public static function set($name, $value, &$config)
    {
        if (is_string($value) && is_file($value)) {
            $value = require $value;
        }
        if (!empty($value)) {
            $config[self::getKey($name)] = $value;
        }

        return $config;
    }

    public static function get($name)
    {
        $config = config('database.');
        $key = self::getKey($name);
        if (!empty($config[$key])) {
            return $config[$key];
        }

        return $config;
    }

    /**
     * @param $name
     *
     * @return string
     */
    protected static function getKey($name)
    {
        return 'db_'.Str::lower($name);
    }
}