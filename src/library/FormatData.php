<?php

namespace modoufuture\modoucore\library;

class FormatData
{
    protected $ori_data = [];
    protected $data = [];

    public function __construct($data)
    {
        $this->ori_data = $data;
        $this->data = $data;
    }

    /**
     * 格式化二维数组
     * @param array $format
     * @param null  $data
     *
     * @return array|null
     */
    public function formatArray($format = [], $data=null)
    {
        if ($data === null) {
            $data = $this->data;
        }

        foreach($data as $k=>&$row) {
            $row = $this->format($row, $format);
        }
        unset($row);
        $this->data = $data;

        return $data;
    }

    /**
     * 格式化一维数组
     * @param array $format
     * @param null  $data
     *
     * @return array|null
     */
    public function format($format=[], $data=null)
    {
        if ($data === null) {
            $data = $this->data;
        }
        foreach($format as $k=>$v) {
            if (is_numeric($k) && !is_numeric($v)) {
                $k = $v;
            }
            if (array_key_exists($k, $data)) {
                $data[$k] = $this->call($k, $data[$k]);
            }
        }
        $this->data = $data;

        return $data;
    }

    /**
     * 调用
     * @param $name
     * @param $data
     *
     * @return mixed
     */
    protected function call($name, $data)
    {
        if (method_exists($this, $name)) {
            return $this->$name($data);
        } elseif (function_exists($name)) {
            return call_user_func($name, $data);
        } else {
            return $data;
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getOriData()
    {
        return $this->ori_data;
    }
}