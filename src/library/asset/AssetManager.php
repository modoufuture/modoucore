<?php

namespace modoufuture\modoucore\library\asset;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Asset\UrlPackage;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;

class AssetManager
{
    /**
     * @var static $inst
     */
    private static $inst = null;
    protected $package;

    private function __construct($config = [])
    {
        if (app()->env->get('app_debug')) {
            $versionStrategy = new DateVersionStrategy();
        } else {
            $versionStrategy = new StaticVersionStrategy(mconfig('version', '0.0.01'));
        }
        $default = new Package($versionStrategy);
        $cdns = mconfig('asset.cdn', []);
        foreach ($cdns as $k=>&$v) {
            $v = new UrlPackage($v, $versionStrategy);
        }
        unset($v);

        $this->package = new Packages($default, $cdns);
    }

    /**
     * @param array $config
     * @return AssetManager
     */
    public static function instance($config = [])
    {
        if (!static::$inst) {
            static::$inst = new self($config);
        }

        return static::$inst;
    }

    /**
     * @param $file
     * @param null|string $type
     * @return string
     */
    public function getUrl($file, $type = null)
    {
        return $this->package->getUrl($file, $type);
    }
}