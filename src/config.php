<?php

return [
    'safe' => [
    ],
    'upload' => [
    ],
    'addons' => [
        'enable' => true,
        'tmp_path' => 'runtime/addons/',
        'save_path' => 'storage/addons/',
        'repositories' => [
            'base' => 'https://addons.modoufuture.com/api/v1'
        ],
        'auth' => [
            'base' => false,
        ],
        'events' => [
            'install_init' => [],
            'install_unpack' => [],
            'service_install' => [],
        ]
    ]
];