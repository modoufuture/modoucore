<?php

namespace modoufuture\modoucore\addons;

abstract class Addons implements \modoufuture\components\addons\Addons
{
    protected $error = '';

    public function backup()
    {
        return true;
    }

    public function upgrade()
    {
        return true;
    }

    public function getError()
    {
        return $this->error;
    }
}