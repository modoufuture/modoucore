<?php

namespace modoufuture\modoucore\command;

use modoufuture\components\addons\Package;
use modoufuture\components\addons\Service;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Db;

class Install extends Command
{
    protected $service;

    protected function configure()
    {
        // 指令配置
        $this->setName('install');
        $this->setDescription('install');
        // 设置参数
        $this->addOption('overwrite', 'o', Option::VALUE_OPTIONAL, 'overwrite install');
        $this->addOption('module', null, Option::VALUE_OPTIONAL, 'custorm install modules');
    }

    protected function execute(Input $input, Output $output)
    {
        $overwrite = $input->hasOption('overwrite') ? $input->getOption('overwrite'):false;
        $module = $input->hasOption('module') ? $input->getOption('module') : '';
        if (!$overwrite && Db::name('migrations')) {
            $output->error('already installed');
            return;
        }
        $this->service = $this->getService();
        $system_module = 'module,options,dict';
        // 先安装模块
        $this->installSystem();
        //$this->installModule();
    }

    protected function installSystem()
    {
        $module = 'module'; // module,column,log,ucenter,manager,language,attachment,model,template
        $root = app()->getRootPath();
        Package::setService($this->getService());
        Package::setPath($root.mconfig('addons.tmp_path'));
        $package = new Package('module', 'module');
        $info = $package->info();
        $package->requires($info);
        $package->download();
        $package->unpack();
        if ($error = $package->verify(true)) {
            throw new \Exception(implode("\n", $error));
        }
        
    }

    protected function installModule($modules)
    {
        $modules = is_array($modules) ? $modules : explode(',', $modules);
        if (empty($modules)) {
            return true;
        }
        $provider = $this->service->getProvider('module');
        foreach($modules as $k=>$v) {
            if (!$provider->install($v, true)) {
                $this->output->error('module '.$v.' install failed!');
            }
        }
    }

    protected function getService()
    {
        return $service = new Service(mconfig('addons.'));
    }
}