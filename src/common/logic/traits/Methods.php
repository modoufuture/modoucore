<?php

namespace modoufuture\modoucore\common\logic\traits;

use think\Db;

trait Methods
{
    public function index($params)
    {
        $where = [];
        $order = 'id desc';
        $lists = $this->model->getApiList($where, $order, $field = '*', $this->page);

        return success('success', $lists['data'], $lists);
    }

    /**
     * @param        $data
     * @param string $scene
     * @param array  $join
     * @param array  $setter
     *
     * @return array
     */
    public function add($data, $scene='add', $join=[], $setter=[])
    {
        Db::startTrans();
        try {
            $this->beforeAdd($data, $scene, $join, $setter);
            $res = $this->model->add($data, $join, $setter);
            if ($res === false) {
                throw new \Exception('add failed');
            }
            Db::commit();

            return success('add success', $res);
        } catch (\Exception $e) {
            Db::rollback();
            return failed($e->getMessage());
        }
    }

    protected function beforeAdd($data, $scene='add', $join=[], $setter=[])
    {
        $validate = get_validate($this->name);
        $scene = $validate->hasScene($scene) ? $scene : '';
        if (is_numeric(key($data)) && is_array(current($data))) { // 这种形式认为是批量修改的数据
            $res = [];
            $validateError = [];
            foreach($data as $k=>$v) {
                if (!$validate->batch(true)->check($v, [], $scene)) {
                    $validateError[$k] = implode("\n", $validate->getError());
                }
            }
            if (!empty($validateError)) {
                throw new \Exception(implode(" --- ", $validateError));
            }
        } else {
            if (!$validate->batch(true)->check($data, [], $scene)) {
                throw new \Exception(implode("\n", $validate->getError()));
            }
        }
    }

    /**
     * @param        $data
     * @param string $scene
     * @param array  $join
     * @param array  $setter
     *
     * @return array
     */
    public function edit($data, $scene='edit', $join=[], $setter=[])
    {
        Db::startTrans();
        try {
            $this->beforeEdit($data, $scene, $join, $setter);
            $res = $this->model->edit($data, $join, $setter);
            Db::commit();

            return success('edit success', $res);
        } catch (\Exception $e) {
            Db::rollback();

            return failed($e->getMessage()?:'edit failed');
        }
    }

    protected function beforeEdit($data, $scene='edit', $join=[], $setter=[])
    {
        $validate = get_validate($this->name);
        $scene = $validate->hasScene($scene) ? $scene : '';
        if (is_numeric(key($data)) && is_array(current($data))) { // 这种形式认为是批量修改的数据
            $res = [];
            $validateError = [];
            foreach($data as $k=>$v) {
                if (!$validate->batch(true)->check($v, [], $scene)) {
                    $validateError[$k] = implode("\n", $validate->getError());
                }
            }
            if (!empty($validateError)) {
                throw new \Exception(implode(" --- ", $validateError));
            }
        } else {
            $info = $this->model->where('id','eq', $data['id'])->find();
            if (empty($info)) {
                throw new \Exception('No Results were found');
            }
            if (empty($data)) {
                throw new \Exception('Data can not be empty.');
            }

            if (!$validate->batch(true)->check($data, [], $scene)) {
                throw new \Exception(implode("\n", $validate->getError()));
            }
        }
    }

    /**
     * @param      $data
     * @param bool $force
     *
     * @return array
     */
    public function delete($data, $force=true)
    {
        $ids = is_array($data['id'])?$data['id'] : explode(',', $data['id']);
        $ids = array_filter($ids, function($item){return is_numeric($item);});
        if (empty($ids)) {
            return failed('invalid id');
        }
        Db::startTrans();
        try {
            $res = [];
            foreach($this->model->where('id', 'in', $ids)->cursor() as $model) {
                $res[$model->id] = $model->delete($force);
            }
            $msg = $force ? 'delete' : 'trash';
            Db::commit();
            if (!empty($res) && false === array_search(true, $res)) {
                return failed($msg.' failed');
            }

            return success($msg.' success');
        } catch (\Exception $e) {
            Db::rollback();
            return failed($e->getMessage());
        }
    }

    public function recycle($params)
    {
        $where = [];
        $order = 'id desc';
        $lists = $this->model->onlyTrashed()->getApiList($where, $order, $field = '*', $this->page);

        return success('success', $lists['data'], $lists);
    }

    public function restore($data)
    {
        $ids = is_array($data['id'])?$data['id'] : explode(',', $data['id']);
        $ids = array_filter($ids, function($item){return is_numeric($item);});
        if (empty($ids)) {
            return failed('invalid id');
        }
        Db::startTrans();
        try {
            $res = [];
            foreach($this->model->onlyTrashed()->where('id', 'in', $ids)->cursor() as $model) {
                $res[$model->id] = $model->restore();
            }
            Db::commit();
            if (!empty($res) && false === array_search(true, $res)) {
                return failed('restore failed');
            }

            return success('restore success', $res);
        } catch (\Exception $e) {
            Db::rollback();
            return failed($e->getMessage());
        }
    }

    public function sort($params)
    {
        $data = [];
        foreach($params as $k=>$v) {
            if (is_numeric($k)) {
                $data[] = ['id'=>$k, 'sort'=>$v<0?0:intval($v)];
            }
        }
        if (empty($data)) {
            return failed('invalid args');
        }
        Db::startTrans();
        try {
            $res = [];
            foreach($data as $k=>$v) {
                $res[$v['id']] = $this->model->isUpdate(true)->save($v, ['id'=>$v['id']]);
            }
            if (!array_filter($res)) {
                return failed('sort failed');
            }
            Db::commit();
            return success('sort success', $res);
        } catch (\Exception $e) {
            Db::rollback();

            return failed($e->getMessage());
        }
    }

    public function status($params)
    {
        $data = [];
        foreach($params as $k=>$v) {
            if (is_numeric($k)) {
                $data[] = ['id'=>$k, 'status'=>$v];
            }
        }
        if (empty($data)) {
            return failed('invalid args');
        }
        Db::startTrans();
        try {
            $res = [];
            foreach($data as $k=>$v) {
                $res[$v['id']] = $this->model->isUpdate(true)->save($v, ['id'=>$v['id']]);
            }
            if (!array_filter($res)) {
                return failed('update failed');
            }
            Db::commit();
            return success('update success', $res);
        } catch (\Exception $e) {
            Db::rollback();

            return failed($e->getMessage());
        }
    }
}