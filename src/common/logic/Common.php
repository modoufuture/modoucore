<?php

namespace modoufuture\modoucore\common\logic;

use modoufuture\modoucore\common\logic\traits\Methods;

class Common
{
    use Methods;
    /**
     * @var string $name 类名，用于获取实例
     */
    protected $name;
    /**
     * @var mixed|\think\App
     */
    protected $app;
    /**
     * @var \modoufuture\modoucore\common\model\Common|object $model
     */
    protected $model;
    /**
     * @var string $error
     */
    protected $error = '';
    protected $page = 1;
    
    public function __construct($name='')
    {
        $this->name = $name?:get_class_name($this);
        $this->app = app();
        $this->model = get_model($this->name);
        $this->page = $this->app->request->param('page', 1, 'intval');
    }

    final public function getError()
    {
        return $this->error;
    }
}