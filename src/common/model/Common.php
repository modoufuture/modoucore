<?php

namespace modoufuture\modoucore\common\model;

use modoufuture\modoucore\common\model\traits\Methods as MethodsModel;
use think\facade\Cache;
use think\Model;
use think\model\concern\SoftDelete;

class Common extends Model
{
    use MethodsModel;
    /**
     * @var array|object $user 用户信息
     */
    protected static $user = [];
    /**
     * @var string $cacheTag 缓存标签
     */
    protected $cacheTag = 'common';

    /**
     * @var array $insert 插入时自动赋值的字段
     */
    protected $insert = ['create_time', 'create_uid', 'create_name'];
    /**
     * @var array $auto 插入，修改时自动赋值的字段
     */
    protected $auto = ['update_time', 'update_uid', 'update_name'];
    protected $deleteTime = null;

    /**
     * Base constructor.
     * 重载父类方法，以便支持动态表名
     * // @todo 调用Query中的方法时会失败
     * @param array $data
     */
    public function __construct($data = [])
    {
        $args = func_get_args();
        if (!empty($args[1])) {
            $this->name = $args[1];
        }
        parent::__construct($data);
    }

    /**
     * 设置时间
     * @param mixed $value
     * @return string
     */
    protected function setCreateTimeAttr($value)
    {
        return $value ? $value : date('Y-m-d H:i:s');
    }
    /**
     * 设置时间
     * @param mixed $value
     * @return string
     */
    protected function setUpdateTimeAttr($value)
    {
        return $value ? $value : date('Y-m-d H:i:s');
    }

    protected function setCreateUidAttr($value)
    {
        return static::user('id', 0);
    }

    protected function setUpdateUidAttr($value)
    {
        return static::user('id', 0);
    }

    protected function setCreateNameAttr($value)
    {
        return static::user('username', 0);
    }

    protected function setUpdateNameAttr($value)
    {
        return static::user('username', 0);
    }

    /**
     * @param      $key
     * @param null $expire
     *
     * @return \modoufuture\modoucore\common\model\Common
     */
    protected function getCache($key, $expire = null)
    {
        return $this->cache($key, $expire, $this->cacheTag);
    }

    /**
     * @param array|object $user
     */
    final public static function setUser($user)
    {
        static::$user = $user;
    }

    /**
     * @param null|string $field
     * @param null|mixed $default
     *
     * @return array|mixed|object|null
     */
    final public static function user($field=null, $default=null)
    {
        if (empty($field)) {
            return static::$user;
        } else {
            return isset(static::$user[$field]) ? static::$user[$field] : $default;
        }
    }
}