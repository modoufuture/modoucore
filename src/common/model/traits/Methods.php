<?php

namespace modoufuture\modoucore\common\model\traits;

use modoufuture\modoucore\library\FormatData;
use modoufuture\utils\Tree;

/**
 * Trait Methods
 *
 * @package app\common\model\traits
 * @mixin \app\common\model\Common
 */
trait Methods
{
    /**
     * @var array $join 默认要关联的表
     * @example [
     *      ['admin_user', 'user.id=admin_user.id', 'LEFT'],  // [关联表, 关联条件, 关联方式]
     * ]
     */
    protected $join = [];
    /**
     * @var array $transAttr 要转换的字段
     */
    protected $transAttr = [];

    /**
     * @param       $data
     * @param array $join   关联的表,需要按照顺序 ['table2'=>['rel'=>['uid'=>'id', 'classid'=>'table1.classid'], 'join'=>[], 'format'=>[]]]
     * @param array $setter  当前表的字段处理 例如： ['ip'=>'ip', 'admin_uid'=>'admin_uid', 'content'=>'json_encode']
     * @param null  $oriData  原始数据，不需要传
     * @param array $models  关联的模型，不需要传
     *
     * @return bool|string
     * @throws \think\exception\PDOException
     * @throws \Exception
     */
    public function add($data, $join = [], $setter = [], $oriData = null, &$models=[])
    {
        if (is_null($oriData)) {
            $oriData = $data;
        }
        // 此处只是简单判断,如果当前的数据下标是一个数字， 就理解为多维数组， 需要执行 addAll方法
        if (is_numeric(key($data)) && is_array(current($data))) {
            return $this->addAll($data, $join, $setter, $oriData, $models);
        }
        if ($setter) {
            $format = new FormatData($data);
            $data = $format->format($setter);
        }
        $res = $this->isUpdate(false)->allowField(true)->save($data);
        $models[$this->name] = $this;
        if ($res === false) {
            throw new \Exception('add failed');
        }
        foreach($join as $k=>$v) {
            $model = get_model($k);
            $tableData = empty($data[$k]) ? [] : $data[$k];
            $tmp = [];
            foreach($v['rel'] as $lk => $rk) {
                if (stripos($rk, '.') !== false) {
                    list($table, $field) = explode('.', $rk);
                    $tmp[$lk] = isset($models[$table]) ? $models[$table]->getAttr($field) : $this->getAttr($field);
                } else {
                    $tmp[$lk] = $this->getAttr($rk);
                }
            }
            if (is_numeric(key($tableData)) && is_array(current($tableData))) { // 同样是二维数据
                $tableData = array_map(function($item) use($tmp){
                    return array_merge($item, $tmp);
                }, $tableData);
            }
            $oriData[$k] = $tableData;
            $res = $model->add($tableData, empty($v['join'])?[]:$v['join'], empty($v['format'])?[]:$v['format'], $oriData, $models);
        }

        return true;
    }

    /**
     * @param       $data
     * @param array $join
     * @param array $setter
     * @param null  $oriData
     * @param array $models
     *
     * @return bool|\think\Collection
     * @throws \think\exception\PDOException
     */
    public function addAll($data, $join = [], $setter = [], $oriData = null, &$models=[])
    {
        if (is_null($oriData)) {
            $oriData = $data;
        }
        if ($setter) {
            $format = new FormatData($data);
            $data = $format->formatArray($setter);
        }
        $res = $this->isUpdate(false)->allowField(true)->saveAll($data);
        $models[$this->name] = $this;
        foreach($join as $k=>$v) {
            $model = get_model($k);
            $tableData = empty($data[$k]) ? [] : $data[$k];
            $tmp = [];
            foreach($v['rel'] as $lk => $rk) {
                if (stripos($rk, '.') !== false) {
                    list($table, $field) = explode('.', $rk);
                    $tmp[$lk] = isset($models[$table]) ? $models[$table]->getAttr($field) : $this->getAttr($field);
                } else {
                    $tmp[$lk] = $this->getAttr($rk);
                }
            }
            if (is_numeric(key($tableData)) && is_array(current($tableData))) { // 同样是二维数据
                $tableData = array_map(function($item) use($tmp){
                    return array_merge($item, $tmp);
                }, $tableData);
            }
            $oriData[$k] = $tableData;

            $model->add($tableData, empty($v['join'])?[]:$v['join'], empty($v['format'])?[]:$v['format'], $oriData, $models);
        }

        return $res;
    }

    /**
     * @param array  $where
     * @param string $order
     * @param string $field
     * @param int    $page
     * @param null   $limit
     * @param string $group
     * @param string $having
     * @param bool $all
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getApiList($where = [], $order = '', $field = '*', $page=1, $limit=20, $group='', $having='', $all=false)
    {
        $model = $this;
        !empty($this->join) && $model = $model->join($this->join);
        $model = $model->where($where)->order($order)->field($field);
        !empty($group) && $model->group($group);
        !empty($having) && $model->having($having);
        if (!empty($this->hidden)) {
            $model = $model->hidden($this->hidden);
        }
        $total = $model->count();
        $model = $this->transAttrs($model);

        if ($all) {
            $page = 1;
            $limit = 'all';
            $maxPage = 1;
            $result = $model->select();
        } else {
            $page = intval($page) <= 1 ? 1 : intval($page);
            $limit = intval($limit)<=0 ? $limit : intval($limit);
            $maxPage = $limit ? ceil($total/$limit) : 1;
            $result = $model->page($page, $limit)->select();
        }

        return [
            'count' => $total,
            'page' => $page > $maxPage ? $maxPage : $page,
            'maxPage' => $maxPage,
            'limit' => $limit,
            'data' => $result
        ];
    }

    public function getChildren($pid, $where = [], $recursive = true, $self=false, $field="*", $order='', $parentField='pid')
    {
        $field .= ',id,'.$parentField;
        $info = $this->where('id', 'eq', $pid)->field($field)->find();
        if (!$recursive) {
            $lists = $this->where($parentField, 'eq', $pid)->where($where)->order($order)->field($field)->select();
        } else {
            $parentPid = empty($info) ? 0 : $info[$parentField];
            $lists = $this->where($where)->where($parentField, 'neq', $parentPid)->order($order)->field('id,'.$parentField)->select()->toArray();
            $tree = new Tree(['pid'=>$parentField, 'name'=>'id', 'pad_name'=>'test', 'flat'=>true]);
            $lists = $tree->getTree($lists, $pid);
            $lists = array_filter($lists, function($item) use($pid){
                return stripos($item['path'], $pid.',') === 0;
            });
            $ids = array_column($lists, 'id', 'id');
            if ($ids) {
                $lists = $this->where('id', 'in', $ids)->order($order)->field($field)->select();
            } else {
                $lists = [];
            }
        }
        $self && $lists->unshift($info);

        return $lists;
    }

    /**
     * @param       $data
     * @param array $join
     * @param array $setter
     * @param null  $oriData
     * @param array $models
     *
     * @return bool|\think\Collection
     * @throws \think\exception\PDOException
     */
    public function edit($data, $join=[], $setter=[], $oriData = null, &$models=[])
    {
        if (is_null($oriData)) {
            $oriData = $data;
        }
        // 此处只是简单判断,如果当前的数据下标是一个数字， 就理解为多维数组， 需要执行 addAll方法
        if (is_numeric(key($data)) && is_array(current($data))) {
            return $this->editAll($data, $join, $setter, $oriData, $models);
        }
        if ($setter) {
            $format = new FormatData($data);
            $data = $format->format($setter);
        }
        $res = $this->isUpdate(false)->allowField(true)->save($data, ['id'=>$data['id']]);
        $models[$this->name] = $this;
        if ($res === false) {
            throw new \Exception('edit failed');
        }
        foreach($join as $k=>$v) {
            $model = get_model($k);
            $tableData = empty($data[$k]) ? [] : $data[$k];
            $tmp = [];
            foreach($v['rel'] as $lk => $rk) {
                if (stripos($rk, '.') !== false) {
                    list($table, $field) = explode('.', $rk);
                    $tmp[$lk] = isset($models[$table]) ? $models[$table]->getAttr($field) : $this->getAttr($field);
                } else {
                    $tmp[$lk] = $this->getAttr($rk);
                }
            }
            if (is_numeric(key($tableData)) && is_array(current($tableData))) { // 同样是二维数据
                $tableData = array_map(function($item) use($tmp){
                    return array_merge($item, $tmp);
                }, $tableData);
            }
            $oriData[$k] = $tableData;
            $res = $model->edit($tableData, empty($v['join'])?[]:$v['join'], empty($v['format'])?[]:$v['format'], $oriData, $models);
        }

        return true;
    }

    /**
     * @param       $data
     * @param array $join
     * @param array $setter
     * @param null  $oriData
     * @param array $models
     *
     * @return bool|\think\Collection
     * @throws \think\exception\PDOException
     */
    public function editAll($data, $join = [], $setter = [], $oriData = null, &$models=[])
    {
        if (is_null($oriData)) {
            $oriData = $data;
        }
        if ($setter) {
            $format = new FormatData($data);
            $data = $format->formatArray($setter);
        }
        $res = $this->saveAll($data, true);
        $models[$this->name] = $this;
        foreach($join as $k=>$v) {
            $model = get_model($k);
            $tableData = empty($data[$k]) ? [] : $data[$k];
            $tmp = [];
            foreach($v['rel'] as $lk => $rk) {
                if (stripos($rk, '.') !== false) {
                    list($table, $field) = explode('.', $rk);
                    $tmp[$lk] = isset($models[$table]) ? $models[$table]->getAttr($field) : $this->getAttr($field);
                } else {
                    $tmp[$lk] = $this->getAttr($rk);
                }
            }
            if (is_numeric(key($tableData)) && is_array(current($tableData))) { // 同样是二维数据
                $tableData = array_map(function($item) use($tmp){
                    return array_merge($item, $tmp);
                }, $tableData);
            }
            $oriData[$k] = $tableData;

            $model->edit($tableData, empty($v['join'])?[]:$v['join'], empty($v['format'])?[]:$v['format'], $oriData, $models);
        }

        return $res;
    }

    /**
     * @param array $transAttr
     *
     * @return $this
     */
    final public function setTransAttr($transAttr = [])
    {
        $this->transAttr = $transAttr;

        return $this;
    }

    /**
     * @param \think\db\Query $query
     *
     * @return mixed
     */
    final protected function transAttrs($query)
    {
        if (!$this->transAttr) {
            return $query;
        }
        $append = array_merge($this->append, array_keys($this->transAttr));
        $query = $query->append($append);
        foreach($this->transAttr as $field=>$value) {
            $query->withAttr($field, function($v, $data) use($value){
                if ($value instanceof \Closure) {
                    return $value($v, $data);
                } elseif (is_string($value) && !isset($data[$value]) && function_exists($value)) {
                    return $value($v, $data);
                } elseif (is_string($value) && isset($data[$value])) {
                    return $data[$value];
                } elseif (is_array($value) && !empty($value['field']) && isset($data[$value['field']])) {
                    if (!empty($value['callback']) && is_callable($value['callback'])) {
                        return $value['callback']($data[$value['field']], $data);
                    } else {
                        return isset($data[$value['field']]) ? $data[$value['field']] : null;
                    }
                }

                return null;
            });
        }

        return $query;
    }
}