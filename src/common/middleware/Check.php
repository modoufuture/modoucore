<?php

namespace modoufuture\modoucore\common\middleware;

use think\Request;

class Check
{
    public function handle(Request $request, \Closure $next)
    {
        get_option('deny_ip', 'safe', []);
    }
}