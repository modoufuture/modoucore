<?php

namespace modoufuture\modoucore\common\controller\traits;

trait Backend
{
    /**
     * 获取列表
     * @return \think\response\Json
     */
    public function index()
    {
        return $this->logic->index($this->params);
    }

    /**
     * 添加操作
     */
    public function add()
    {
        return $this->logic->add($this->params);
    }

    /**
     * 修改操作
     * @return mixed
     */
    public function edit()
    {
        return $this->logic->edit($this->params);
    }

    /**
     * 删除操作
     * @return mixed
     */
    public function delete()
    {
        return $this->logic->delete($this->params);
    }
}