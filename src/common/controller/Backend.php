<?php

namespace modoufuture\modoucore\common\controller;

use modoufuture\modoucore\common\controller\traits\Backend as TraitBackend;

class Backend extends Common
{
    use TraitBackend;

    protected $appId = 'backend';
}