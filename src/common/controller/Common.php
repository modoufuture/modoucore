<?php

namespace modoufuture\modoucore\common\controller;

use think\Controller;

class Common extends Controller
{
    protected $appId = 'common';
    /**
     * @var object|\modoufuture\modoucore\common\logic\Common
     */
    protected $logic;
    /**
     * @var array $params 请求参数
     */
    protected $params;

    protected function initialize()
    {
        $this->logic = get_logic($this->logic?:get_class_name($this));
        $this->params = $this->request->except('s');
    }
}